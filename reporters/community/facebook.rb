require 'koala'
require "date"

require_relative '../config.rb'

# initialize a API connection, for instance
@graph = Koala::Facebook::API.new(FB_TOKEN)

# return number of total likes and updatet time
def likes_count
	facebook = Array.new
	timestamp = DateTime.now.strftime("%Y-%m-%dT%H:%M:%SZ")
	likes = @graph.get_object(FB_PAGE)["likes"]
	facebook.push timestamp: timestamp, likes_count: likes
	facebook
end

# return number of total likes and update time
# if there is new like, return likes 
def likes_timeline(likes_count = 0)
	likes = @graph.get_object(FB_PAGE)["likes"]
	if (likes_count != likes)
		facebook = Array.new
		# timestamp = DateTime.now.strftime("%Y-%m-%dT%H:%M:%SZ")
		timestamp = Time.now - (60*60)
		facebook.push timestamp: timestamp, count: likes
	end
end


# get number of talking about
def total_talks
	@graph.get_object(FB_PAGE)["talking_about_count"]	
end

# get likes by country
def likes_by_country
	@graph.get_connections(FB_PAGE, "Insights")	
end

# cannot get a user list who have liked page... following... cannot get number of likes by date
# https://developers.facebook.com/x/bugs/147185208750426/