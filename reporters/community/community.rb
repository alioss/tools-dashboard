require '../config.rb'

require 'rubygems'
require 'bundler/setup'

require 'rest_client'
require 'json'
#require 'awesome_print'
require 'benchmark'


require 'date'
require_relative 'twitter.rb'
require_relative 'facebook.rb'


$server = RestClient::Resource.new(SERVER_URL)

#report = JSON.parse($server["/reports/%i/versions/latest.json"%COMMUNITY_REPORT_ID].get, symbolize_names: true)[:data]
report = { }

# facebook likes
likes_count = 0

loop {
	report[:recent_activity] = []

	# new tweets
	puts Benchmark.measure {
		report[:recent_activity] += show_recent_posts(TWITTER_TAG+" -rt", 4).map { |tweet| 
			tweet[:timestamp] = tweet[:timestamp].strftime("%Y-%m-%dT%H:%M:%SZ")
			tweet[:system] = "twitter"
			tweet[:id] = tweet[:url]
			tweet[:magnitude] = 1+tweet[:retweet_count]
			tweet
		}
	}

	# facebook likes - check if there are new likes - if yes, then send data to charts
	puts Benchmark.measure {
		l = likes_timeline(likes_count)
		if l.class == Array then
			report[:recent_activity] += l.map { |likes|
				likes[:timestamp] = likes[:timestamp].strftime("%Y-%m-%dT%H:%M:%SZ")
				likes[:system] = "facebook"
				likes[:magnitude] = 1
				likes_count = likes[:id] = likes[:count]
				p likes[:count]
				p likes[:timestamp]
				likes
			}
		end
	}

	puts "Uploaded report: %i"%JSON.parse($server["/reports/%i/versions.json"%COMMUNITY_REPORT_ID].post(content: JSON.dump({ data: report })), symbolize_names: true)[:version]

	sleep 10
}
